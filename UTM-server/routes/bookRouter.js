var express = require('express');
var book = express.Router();
var Book=require("../models/book");
var mongoose=require("mongoose");

/* GET users listing. */
book.route("/")
.get(function(req, res, next) {
	console.log(req.query);
	Book.find(req.query,function(err,std){
		if(err)
			return next(err);
		res.status(200).json(std);
	});
})
.post(function(req,res,next){
	var obj={"bookid":req.body.bookid};
	Book.findOne(obj,function(err,fac){
		if(err)
			return next(err);
		console.log(req.body);
		if(!fac){
			Book.create(req.body,function(err,std){
				if(err)
					return next(err);
			console.log(fac);
				res.status(200).json("Register Sucessfull");
			});
		}
		else{
			res.status(501).json("User already exists");
		}
	
		
	});
})
.put(function(req, res, next) {
	Book.update(req.query,{$set:req.body},{new:true},function(err,std){
		
		console.log(req.body);
		console.log(std);
		res.status(200).json(std);
	});
})
.delete(function(req, res, next) {
	Book.remove(req.query,function(err,std){
		if(err)
			return next(err);
		console.log(req.body);
		console.log(std);
		res.status(200).json(std);
	});
});

module.exports = book;
