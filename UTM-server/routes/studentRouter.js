var express = require('express');
var student = express.Router();
var passport=require("passport");
var Student=require("../models/student");
var mongoose=require("mongoose");

/* GET users listing. */
student.route("/")
.get(function(req, res, next) {
	console.log(req.query);
	Student.find(req.query).populate("book_taken").exec(function(err,std){
		if(err)
			return next(err);
		res.status(200).json(std);
	});
})
.put(function(req, res, next) {
	Student.update(req.query,{$set:req.body},{new:true},function(err,std){
		
		console.log(req.body);
		console.log(std);
		res.status(200).json(std);
	});
})
.delete(function(req, res, next) {
	Student.remove(req.query,function(err,std){
		if(err)
			return next(err);
		console.log(req.body);
		console.log(std);
		res.status(200).json(std);
	});
});
student.route("/:studentId")
.get(function(req, res, next) {
	Student.find(req.params.studentId,function(err,std){
		if(err)
			return next(err);
		res.status(200).json(std);
	});
})
.put(function(req, res, next) {
	Student.findByIdAndUpdate(req.params.studentId,{$set:req.body},{new:true},function(err,std){
		if(err)
			return next(err);
		console.log(req.body);
		console.log(std);
		res.status(200).json(std);
	});
})
.delete(function(req, res, next) {
	Student.FindByIdAndRemove(req.params.studentId,function(err,std){
		if(err)
			return next(err);
		res.status(200).json(std);
	});
});
student.route("/result")

.post(function(req, res, next) {
	Student.findOne(req.query,function(err,std){
		if(err)
			return next(err);
		std.results.push(req.body);
		std.save(function(err,std){
			if(err)
				return next(err);
			res.status(200).json(std);
		});
		
	});
});
student.route("/books")
.post(function(req, res, next) {
	Student.findOne(req.query,function(err,std){
		console.log(req.body);
		
		if(!std){
		std.book_issued.book_taken=req.body;
		std.save(function(err,std){
			if(err)
				return next(err);
			res.status(200).json(std);
		});
		}
	});
});
student.route("/:studentId/result/:resultId")

.put(function(req, res, next) {
	Student.findById(req.params.studentId,function(err,std){
		if(err)
			return next(err);
		std.results.id(req.params.resultId).remove();
		std.results.push(req.body);
		std.save(function(err,std){
			if(err)
				return next(err);
			res.status(200).json(std);
		});
	});
})
.delete(function(req,res,next){
	Student.findById(req.params.studentId,function(err,std){
		if(err)
			return next(err);
		std.results.id(req.params.resultId).remove();
		std.save(function(err,std){
			if(err)
				return next(err);
			res.status(200).json(std);
		});
	});	
});

student.post('/register',function(req, res, next) {
	Student.register(new Student({username:req.body.username}),req.body.password,function(err,std){
		if(err)
			return next(err);
		std.fullname=req.body.fullname;
		std.image=req.body.image;
		std.Branch=req.body.Branch;
		std.roll_number=req.body.roll_number;
		std.semester=req.body.semester;
		std.email=req.body.email;
		std.DOB=req.body.DOB;
		std.book_issued_fine=req.body.book_issued_fine;
		std.registration_fine=req.body.registration_fine;
		std.contact_number=req.body.contact_number;
		std.address=req.body.address;
		std.save(function(err,std){
			if(err)
				return next(err);
			passport.authenticate("local")(req,res,function(){
				return res.status(200).json("Register Succesful");
			});
		});
		
	});
});
student.post('/login',function(req, res, next) {
	passport.authenticate("local",function(err,std,info){
		if(err)
			return next(err);
		if(!std)
			return res.status(401).json({err:info});
		req.logIn(std,function(err){
			if(err)	
				return res.status(401).json("Username or Password is incorrect");
		
		res.status(200).json({
        		status: 'Login successfull!',
        		success: true
      		});
		console.log(std.fullname);			
		});
	})(req,res,next);
});
student.get("/logout",function(req,res,next){
	req.logout();
});

module.exports = student;
