var express=require("express");
var staffRouter=express.Router();
var bodyParser=require("body-parser");
staffRouter.use(bodyParser.json());
var mongoose=require("mongoose");
var passport=require("passport");
var Staff=require("../models/staff");

staffRouter.route("/")
.get(function(req,res,next){
	Staff.find(req.query,function(err,fac){
		if(err)
			return next(err);
		res.status(200).json(fac);
	});
})
.delete(function(req,res,next){
	Staff.remove({},function(err,fac){
		if(err)
			return next(err);
		res.status(200).json(fac);
	});
});
staffRouter.route("/:facultyId")
.get(function(req,res,next){
	Staff.findById(req.params.facultyId,function(err,fac){
		if(err)
			return next(err);
		res.status(200).json(fac);
	});
})
.put(function(req,res,next){
	Staff.findByIdAndUpdate(req.params.facultyId,{$set:req.body},{new:true},function(err,fac){
		if(err)
			return next(err);
		res.status(200).json(fac);
	});
})
.delete(function(req,res,next){
	Staff.findByIdAndRemove(req.params.facultyId,function(err,fac){
		if(err)
			return next(err);
		res.status(200).json(fac);
	});
});
staffRouter.post("/register",function(req,res,next){
	var obj={"username":req.body.username};
	Staff.findOne(obj,function(err,fac){
		if(err)
			return next(err);
		console.log(fac);
		if(!fac){
			Staff.create(req.body,function(err,std){
				if(err)
					return next(err);
			//res.json(req.decoded.admin);
				res.status(200).json("Register Sucessfull");
			});
		}
		else{
			res.status(501).json("User already exists");
		}
	
		
	});
});
staffRouter.post("/login",function(req,res,next){
	var obj={"username":req.body.username,"password":req.body.password,"department":req.body.department};
	Staff.findOne(obj,function(err,fac){
		if(err)
			return next(err);
		if(!fac)
			return res.status(401).json("Incorrect Password or Username");
		else{
			
			res.status(200).json({
				status:"Login Succesful",
				success:true
				
			});
			console.log(fac.admin);
		}
	});
});
staffRouter.get("/logout",function(req,res,next){
	req.logout();
});

module.exports=staffRouter;
