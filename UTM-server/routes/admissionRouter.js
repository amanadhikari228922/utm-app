var express = require('express');
var Admissionrouter = express.Router();
var bodyParser=require("body-parser");
Admissionrouter.use(bodyParser.json());
var Admission=require("../models/admission");
Admissionrouter.post('/', function(req, res, next) {
	var obj={email:req.body.email};
	Admission.findOne(obj,function(err,adm){
		if(err)
			return next(err);
		if(!adm){
			Admission.create(req.body,function(err,result){
				if(err)
					return next(err);
				res.status(200).json(result);
			});
		}
		else{
			res.status(200).json("You are already register");
		}
		
	});
	
});

module.exports = Admissionrouter;
