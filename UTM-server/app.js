var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport=require("passport");
var LocalStrategy=require("passport-local").Strategy;
var mongoose=require("mongoose");
var config=require("./config");
mongoose.connect(config.mongoUrl);
var db=mongoose.connection;
db.on("error",console.error.bind("Connection Error"));
db.once("open",function(){
	console.log("Connection Established");
});
var admissionRouter = require('./routes/admissionRouter');
var staffRouter=require("./routes/staffRouter");
var studentRouter=require("./routes/studentRouter");
var bookRouter=require("./routes/bookRouter");
var app = express();
app.all("*",function(req,res,next){
	if(req.secure)
		return next();
	res.redirect("https://"+req.hostname+":"+app.get("secPort")+req.url);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var Student=require("./models/student");
app.use(passport.initialize());
passport.use(new LocalStrategy(Student.authenticate()));
passport.serializeUser(Student.serializeUser());
passport.deserializeUser(Student.deserializeUser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/admission', admissionRouter);
app.use('/staff', staffRouter);
app.use('/student', studentRouter);
app.use('/book', bookRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
