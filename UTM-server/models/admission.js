var mongoose=require("mongoose");
var Schema=mongoose.Schema;
var admission=new Schema({
	firstname:{
		type:String,
		require:true
	},
	lastname:{
		type:String,
		require:false
	},
	email:{
		type:String,
		require:true
	},
	DOB:{
		type:String,
		require:true
	},
	branch:{
		type:String,
		require:true
	},
	contact_number:{
		type:Number,
		require:true
	},
	address:{
		type:String,
		require:true
	}
});
module.exports=mongoose.model("admission",admission);
