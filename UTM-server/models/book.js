var mongoose=require("mongoose");
var Schema=mongoose.Schema;
var book=new Schema({
	bookid:{
		type:String,
		require:true
	},
	book_name:{
		type:String,
		require:true
	},
	author:{
		type:String,
		require:true
	},
	issued_date:{
		type:String,
		require:false
	},
	return_date:{
		type:String,
		require:false
	},
	issued_by:{
		type:mongoose.Schema.Types.ObjectId,
		ref:"student"
	}
});
module.exports=mongoose.model("book",book);
