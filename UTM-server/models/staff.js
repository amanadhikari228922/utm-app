var mongoose=require("mongoose");
var Schema=mongoose.Schema;

var staff=new Schema({
	username:String,
	passowrd:String,
	department:{
		type:String,
		require:true
	},
	image:{
		type:String,
		require:true
	},
	fullname:{
		type:String,
		require:true
	},
	designation:{
		type:String,
		require:true
	},
	qualification:{
		type:String,
		require:true
	},
	fore_front_area_of_research:{
		type:String,
		require:false,
		default:""
	},
	emailid:{
		type:String,
		require:true
	},
	experience:{
		type:String,
		require:false,
		default:""
	},
	contact_number:{
		type:Number,
		require:true
	},
	award_and_achievements:{
		type:String,
		require:false,
		default:""
	}
});

module.exports=mongoose.model("staff",staff);
