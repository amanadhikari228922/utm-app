angular.module('UTM.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,authFactory) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginStudentData = {};
  $scope.loginStaffData = {};
  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/signin.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeStudentLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.studentlogin = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doStudentLogin = function() {
	console.log('Doing loging', $scope.loginStudentData);
	authFactory.studentLogin($scope.loginStudentData);
	$scope.closeStudentLogin();
   
  };
$ionicModal.fromTemplateUrl('templates/staff_signin.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal1 = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeStaffLogin = function() {
    $scope.modal1.hide();
  };

  // Open the login modal
  $scope.stafflogin = function() {
    $scope.modal1.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doStaffLogin = function() {
   	console.log('Doing loging', $scope.loginStaffData);
	authFactory.staffLogin($scope.loginStaffData);
	$scope.closeStaffLogin();
  };
})

.controller('FacultyController',['$scope','staffFactory','authFactory',"$window","$ionicLoading","$timeout", function($scope,staffFactory,authFactory,$window,$ionicLoading,$timeout) {
	$scope.error="501: Server is Down Currently........";
	$scope.status=true;
	$ionicLoading.show({
		template:"<ion-spinner></ion-spinner> Loading..."
	});
	$timeout(function(){
		$ionicLoading.hide()
	},1000);
	staffFactory.query( function (response) {
	    
            $scope.faculty = response;
	    $scope.status=true;
		console.log(response);
        },
        function (response) {
		$scope.status=false;
		
        });
	console.log($scope.faculty);
	
}])


.controller('StaffController',['$scope','authFactory',"sreFactory","libraryFactory","$window","$ionicPlatform","$cordovaCamera","$cordovaImagePicker","$cordovaToast","$ionicPopup",function($scope,authFactory,sreFactory,libraryFactory,$window,$ionicPlatform, $cordovaCamera,$cordovaImagePicker,$cordovaToast,$ionicPopup) {
	$scope.userData=authFactory.userData();
	$scope.staff=authFactory.staffInfo().query({username:$scope.userData.username}).$promise.then(
		function(response){
			var staff= response;
			$scope.staff=staff[0];
		},
		function(response){
			
		}
	);
	sreFactory.allStudent().query(
		function(response){
			
			$scope.student=response;
		},
		function(response){
			
		}
	);
	$scope.logout=function(){
		$window.location.href="#/app/home";
	};
	libraryFactory.addBook().query(
		function(response){
			
			$scope.book1=response;
		},
		function(response){
			
		}
	);
	$scope.register={};
   $ionicPlatform.ready(function() {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
         $scope.takePicture = function() {
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.register.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });

          

        };
    });
	$scope.book={};
	$scope.addBook=function(){

		libraryFactory.addBook().save($scope.book,function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Added Successfull","long","bottom").then(function(success){},function(error){});
			});			
			
			},
			function(response){
				$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Added","long","bottom").then(function(success){},function(error){});
			});
			});
	};	

        $scope.removeBook={};
	 $scope.removeBook=function(){
		var confirmPopup=$ionicPopup.confirm({
			title:"Confirm Delete",
			template:"Are you sure you want delete this student?"
		});
		confirmPopup.then(function(res){
		if(res){
		console.log($scope.remove);
		libraryFactory.addBook().delete({ book_id:$scope.removeBook.book_id },function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Deleted Successfull","long","bottom").then(function(success){},function(error){});
			});
			
		},function(error){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Deleted","long","bottom").then(function(success){},function(error){});
			});
		});
		}
		});
	};

	 $ionicPlatform.ready(function(){
	$scope.gallery=function(){
		var options1={
			maximumImagesCount:1,
			width:100,
			height:100,
			quality:80
		};
		$cordovaImagePicker.getPictures(options1).then(function(results){
			$scope.register.image=results[0];
		});
	};
   });
        $scope.doRegistration=function(){
		
		console.log($scope.register);

		sreFactory.addStudent().save($scope.register,function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Added Successfull","long","bottom").then(function(success){},function(error){});
			});			
			
			},
			function(response){
				$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Added","long","bottom").then(function(success){},function(error){});
			});
			});
	};
	$scope.update={};
	 $ionicPlatform.ready(function() {
        var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
         $scope.UtakePicture = function() {
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.update.image = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });

          

        };
    });
	 $ionicPlatform.ready(function(){
	$scope.Ugallery=function(){
		var options1={
			maximumImagesCount:1,
			width:100,
			height:100,
			quality:80
		};
		$cordovaImagePicker.getPictures(options1).then(function(results){
			$scope.update.image=results[0];
		});
	};
   });
	 $scope.doUpdateStudent=function(){
		console.log($scope.update);
		sreFactory.updateStudent().update({ username:$scope.update.username },$scope.update,function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Updated","long","bottom");
			});
			
		},function(error){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Updated","long","bottom").then(function(success){},function(error){});
			});
		});
	};
	$scope.remove={};
	 $scope.doRemoveStudent=function(){
		var confirmPopup=$ionicPopup.confirm({
			title:"Confirm Delete",
			template:"Are you sure you want delete this student?"
		});
		confirmPopup.then(function(res){
		if(res){
		console.log($scope.remove);
		sreFactory.removeStudent().delete({ username:$scope.remove.username },function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Deleted Successfull","long","bottom").then(function(success){},function(error){});
			});
			
		},function(error){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Deleted","long","bottom").then(function(success){},function(error){});
			});
		});
		}
		});
	};
	$scope.upload={};
	 $scope.uploadResult=function(){
		console.log($scope.upload);
		sreFactory.uploadResult().save({username:$scope.upload.username},$scope.upload,function(response){
			
				$cordovaToast.show("Uploaded Succesfull","long","bottom");
			
			
		},function(error){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Not Uploaded","long","bottom").then(function(success){},function(error){});
			});
		});
	};
	console.log($scope.staff);
	console.log($scope.userData.username);
}])
.controller('StudentController',['$scope',"studentFactory",'authFactory','libraryFactory',"$window",function($scope,studentFactory,authFactory,libraryFactory,$window) {
	$scope.userData=authFactory.userData();
	$scope.student=authFactory.studentInfo().query({username:$scope.userData.username}).$promise.then(
			function(response){
				var student= response;
				$scope.student=student[0];
			},
			function(response){
		
			}
		);
	libraryFactory.addBook().query(
		function(response){
			
			$scope.book1=response;
		},
		function(response){
			
		}
	);
	$scope.show=false;
	
		$scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
		
	
	$scope.logout=function(){
		$window.location.href="#/app/home";
	};
	console.log($scope.student);
	console.log($scope.userData.username);
}])
.controller("AdmissionController",["$scope","admissionFactory","$ionicPlatform","$cordovaToast",function($scope,admissionFactory,$ionicPlatform,$cordovaToast){
	$scope.admission={firstname:"",lastname:"",email:"",DOB:"",branch:"",contact_number:"",address:""};
	console.log("error");
	$scope.error="501: Server is Down Currently........";
	$scope.status=true;
	$scope.sendAdmission=function(){
		admissionFactory.save($scope.admission,
		function(response){
			$scope.status=true;
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Registered","long","bottom").then(function(success ){},function(){error});
			});
			
		},
		function(response){
			$scope.status=false;
		});
		
	}
}])


.controller('CtrlHosp', ['$scope',function($scope) {
  $scope.groups = [];
  
  $scope.groups = [
    { name: 'Course Overview', id: 1, items:{data : 'The Hospitality, Travel and Tourism sector is a thriving industry in India and worldwide. The field of travel and tourism primarily deals with taking care of tourists, hospitality management, travel management, tour management etc. It offers diverse job opportunities.India is a well- known tourist destination. Heritage, cultural, and medical tourists throng Indian tourist destinations each year. Travel and Tourism management professionals manage the accommodation, travel modes, tour program and overall stay of tourists in India. They also perform the above mentioned tasks abroad, when they take Indian nationals on tours.UTM grooms students through industry-endorsed curriculum and intensive, service-oriented practicals by faculty with industry exposure, in excellent infrastructure. ' }},
      
    { name: 'Eligibility and Admission', id: 1, items: { data: 'Eligibility:   Minimum 45% Marks at Secondary School Examination (10th) and Senior/Higher School Certificate Examination (12th).Admission:   Eligible students are called for a Personal Interview. Students are offered admission on the basis of their performance in Personal Interview.Duration:   3 Years'}},
      
    { name: "<a href='http://www.utm.ac.in/bba-fees-structure.php'  >Fee Structure</a>", id: 1},
    { name: "<a  href='http://utm.ac.in/PDF/4._BBA-HTT.PDF'>Course Structure</a>" },
    { name: 'Loan Assistance', id: 1, items: { data: 'UTM has a tie-up with the Oriental Bank of Commerce to help its students avail education loans. This facility is open to all the students admitted to UTM. Our Admission Team members are always available to guide students with regards to loan documentation. Please visit your nearest branch of Oriental Bank of Commerce for further loan process.Agreement of Oriental Bank of Commerce' }},
  ];

    $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
}])


.controller('CtrlFi', ['$scope',function($scope) {
  $scope.groups = [];
  
  $scope.groups = [
    { name: 'Course Overview', id: 1, items:{data : 'The Bachelor of Business Administration in Finance degree program can provide future finance professionals with an in-depth understanding of how financial planning, investment portfolio, and economic affairs are handled. The knowledge imparted in this program will make the professionals understand the basic concept of financial management, corporate finance and investment banking. This program will lead to career opportunities like financial manager, financial analyst, financial planner, portfolio manager, financial service sales agent, brokers, financial executive, accountant etc. ' }},
      
    { name: 'Eligibility and Admission', id: 1, items: { data: 'Eligibility:   Minimum 45% Marks at Secondary School Examination (10th) and Senior/Higher School Certificate Examination (12th).Admission:   Eligible students are called for a Personal Interview. Students are offered admission on the basis of their performance in Personal Interview.Duration:   3 Years'}},
      
    { name: "<a href='http://www.utm.ac.in/bba-fees-structure.php'  >Fee Structure</a>", id: 1},
    { name:  "<a style='text-decoration:none;' href='http://utm.ac.in/PDF/2._Proposed_BBA-_Finance_Management.PDF'>Course Structure</a>" },
    { name: 'Loan Assistance', id: 1, items: { data: 'UTM has a tie-up with the Oriental Bank of Commerce to help its students avail education loans. This facility is open to all the students admitted to UTM. Our Admission Team members are always available to guide students with regards to loan documentation. Please visit your nearest branch of Oriental Bank of Commerce for further loan process.Agreement of Oriental Bank of Commerce' }},
  ];

    $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
}])

.controller('CtrlMark', ['$scope',function($scope) {
  $scope.groups = [];
  
  $scope.groups = [
    { name: 'Course Overview', id: 1, items:{data : 'UTM offers a unique course in BBA with specialization in Marketing that combines marketing, advertising, and the technology of digital media. Students in our bachelor’s program develop a broad understanding of the practices of Marketing while focusing on key industry practices in traditional and digital communication. UTM students graduate with up-to-date, practical skills that are sought by our placement partners. If you are interested in marketing and marketing communications, this course combines a path towards professional qualifications and business readiness. By providing our students with intensive training in SEO, PPC, social media, mobile and other digital strategies, we endow them with the career skills to take on the future of marketing.  Our courses are taught by experts in marketing, advertising and public relations. They have years of experience working in professional environments, building client relationships and reputations, and developing a strong market presence. You will be able to learn from their expertise throughout your course.   ' }},
      
    { name: 'Eligibility and Admission', id: 1, items: { data: 'Eligibility:   Minimum 45% Marks at Secondary School Examination (10th) and Senior/Higher School Certificate Examination (12th).Admission:   Eligible students are called for a Personal Interview. Students are offered admission on the basis of their performance in Personal Interview.Duration:   3 Years'}},
      
    { name: "<a href='http://www.utm.ac.in/bba-fees-structure.php'  >Fee Structure</a>", id: 1},
    { name:"<a style='text-decoration:none;' href='http://utm.ac.in/PDF/3._Proposed_BBA-_Marketing_Management.PDF'>Course Structure</a>"},
    { name: 'Loan Assistance', id: 1, items: { data: 'UTM has a tie-up with the Oriental Bank of Commerce to help its students avail education loans. This facility is open to all the students admitted to UTM. Our Admission Team members are always available to guide students with regards to loan documentation. Please visit your nearest branch of Oriental Bank of Commerce for further loan process.Agreement of Oriental Bank of Commerce' }},
  ];

    $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
}])


.controller('CtrlRet', ['$scope',function($scope) {
  $scope.groups = [];
  
  $scope.groups = [
    { name: 'Course Overview', id: 1, items:{data : 'Retailing is a booming industry in India. Sprawling shopping centers, multi-storied malls, and huge complexes offer shopping, entertainment, food, and other services all under one roof. The last two decades witnessed the fastest retail industry growth in India ( more than 80%). Good communication skills, extrovert personality, people skills and patience are the necessary prerequisites of a retail professional. Retail management is a good career option for those who have an avid interest in sales markets, business diversification, and believe the notion that ‘Customer is the King’  ' }},
      
    { name: 'Eligibility and Admission', id: 1, items: { data: 'Eligibility:   Minimum 45% Marks at Secondary School Examination (10th) and Senior/Higher School Certificate Examination (12th).Admission:   Eligible students are called for a Personal Interview. Students are offered admission on the basis of their performance in Personal Interview.Duration:   3 Years'}},
      
    { name: "<a href='http://www.utm.ac.in/bba-fees-structure.php'  >Fee Structure</a>", id: 1},
    { name:"<a style='text-decoration:none;' href='http://utm.ac.in/PDF/1._Proposed_BBA-_Retail_Management.PDF'>Course Structure</a>" },
    { name: 'Loan Assistance', id: 1, items: { data: 'UTM has a tie-up with the Oriental Bank of Commerce to help its students avail education loans. This facility is open to all the students admitted to UTM. Our Admission Team members are always available to guide students with regards to loan documentation. Please visit your nearest branch of Oriental Bank of Commerce for further loan process.Agreement of Oriental Bank of Commerce' }},
  ];

    $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
  
}]);
