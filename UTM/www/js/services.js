'use strict';
angular.module('UTM.services',['ngResource'])
.constant("baseURL","https://192.168.43.151:3443/")
.factory('staffFactory',['$resource','baseURL',function($resource,baseURL){
	return $resource(baseURL+"staff",null,{"update":{method:"PUT"}});
	
}])
.factory('studentFactory',['$resource','baseURL',function($resource,baseURL){
	var stdfac={};
	stdfac.issuedBook=function(){
		return $resource(baseURL+"student/books",null,{"update":{method:"PUT"}});	
	};
	return stdfac;
	
}])
.factory('libraryFactory',['$resource','baseURL',function($resource,baseURL){
	var libfac={};
	libfac.addBook=function(){
		return $resource(baseURL+"book",null,{"update":{method:"PUT"}});	
	};
	return libfac;
	
}])
.factory("sreFactory",['$resource','baseURL',function($resource,baseURL){
	var srefac={};
	srefac.addStudent=function(){
		return $resource(baseURL+"student/register",null,{"update":{method:"PUT"}});	
	};
	srefac.updateStudent=function(){
		return $resource(baseURL+"student",null,{"update":{method:"PUT"}});	
	};
	srefac.removeStudent=function(){
		return $resource(baseURL+"student",null,{"update":{method:"PUT"}});	
	};
	srefac.allStudent=function(){
		return $resource(baseURL+"student",null,{"update":{method:"PUT"}});	
	};
	srefac.uploadResult=function(){
		return $resource(baseURL+"student/result",null,{"update":{method:"PUT"}});	
	};
	return srefac;
}])
.factory("admissionFactory",["$resource","baseURL",function($resource,baseURL){
	return $resource(baseURL+"admission",null,{"update":{method:"PUT"}});
}])

.factory("authFactory",["$resource","baseURL","$window","$ionicPlatform","$cordovaToast",function($resource,baseURL,$window,$ionicPlatform,$cordovaToast){
	var authfac={};
	var userData="";
	authfac.studentLogin=function(loginData){
		userData=loginData;
		$resource(baseURL+"student/login").save(loginData,function(response){
			console.log("done");
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Login Succesfull","long","bottom").then(function(success){},function(error){});
			});
			$window.location.href="#/student";
			
		},function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Error: " + response.status + " " + response.statusText,"long","bottom").then(function(success){},function(error){});
			});		
		});
		
	};
	authfac.staffLogin=function(loginData){
		userData=loginData;
		$resource(baseURL+"staff/login").save(loginData,function(response){
			console.log("done");
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Login Succesfull","long","bottom").then(function(success){},function(error){});
			});
			if(loginData.username=="bhairab")
				$window.location.href="#/sre";
			if(loginData.username=="shinoy")
				$window.location.href="#/library";
			
		},function(response){
			$ionicPlatform.ready(function(){
				$cordovaToast.show("Error: " + response.status + " " + response.statusText,"long","bottom").then(function(success){},function(error){});
			});		
		});
		
	};
	
	authfac.userData=function(){
		return userData;
	};
	authfac.studentInfo=function(){
		console.log(userData.username);
		return $resource(baseURL+"student",null,{"update":{method:"PUT"}});
	};
	authfac.staffInfo=function(){
		console.log(userData.username);
		return $resource(baseURL+"staff",null,{"update":{method:"PUT"}});
	};
	return authfac;
}])
;

