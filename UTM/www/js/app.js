// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('UTM', ['ionic', 'ngCordova','UTM.controllers','UTM.services'])

.run(function($ionicPlatform,$rootScope,$ionicLoading) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
$rootScope.$on("loading:show",function(){
	$ionicLoading.show({
		template:"<ionic-spinner></ionic-spinner> Loading..."
	});
});
$rootScope.$on("loading:hide",function(){
	$ionicLoading.hide({
		template:"<ionic-spinner></ionic-spinner> Loading..."
	});
});
$rootScope.$on("$stateChangeStart",function(){
	$rootScope.$broadcast("loading:show");
});
$rootScope.$on("$stateChangeSuccess",function(){
	$rootScope.$broadcast("loading:hide");
})
})
.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidebar.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'mainContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })

  .state('app.aboutus', {
      url: '/aboutus',
      views: {
        'mainContent': {
          templateUrl: 'templates/aboutus.html'
        }
      }
    })
    .state('app.admission', {
      url: '/admission',
      views: {
        'mainContent': {
          templateUrl: 'templates/admission.html',
          controller: 'AdmissionController'
        }
      }
    })
    .state('app.academics', {
      url: '/academics',
      views: {
        'mainContent': {
          templateUrl: 'templates/academics.html',
          controller: ''
        }
      }
    })
    .state('app.faculties', {
      cache: false,
      url: '/faculties',
      views: {
        'mainContent': {
          templateUrl: 'templates/faculties.html',
          controller: 'FacultyController'
        }
      }
    })
    .state('app.contactus', {
      url: '/contactus',
      views: {
        'mainContent': {
          templateUrl: 'templates/contactus.html',
          controller:''
        }
      }
    })
    
    .state('app.bba-hospitality' ,{
           url:'/bba-hospitality',
           views:{
           'mainContent':{
               templateUrl: 'templates/bba-hospitality.html',
               controller: 'CtrlHosp'
           } 
        }
   })
    
.state('app.finance' ,{
           url:'/finance',
           views:{
           'mainContent':{
               templateUrl: 'templates/finance.html',
               controller: 'CtrlFi'
           } 
        }
   })

      .state('app.marketing' ,{
           url:'/marketing',
           views:{
           'mainContent':{
               templateUrl: 'templates/marketing.html',
               controller: 'CtrlMark'
           } 
        }
   })

    .state('app.retail' ,{
           url:'/retail',
           views:{
           'mainContent':{
               templateUrl: 'templates/retail.html',
               controller: 'CtrlRet'
           } 
        }
   });
 $stateProvider
   .state('student', {
    cache: false,
    url: '/student',
    templateUrl: 'templates/student.html',
    controller: 'StudentController'
  })
  .state('result' ,{
           url:'/result',
	   cache: false,
           templateUrl: 'templates/result.html',
           controller: 'StudentController'
         
   })
  .state('sre' ,{
           url:'/sre',
           templateUrl: 'templates/sre.html',
           controller: 'StaffController'
         
   })
   .state('add_student' ,{
           url:'/add_student',
           templateUrl: 'templates/add_student.html',
           controller: 'StaffController'
         
   })
  .state('update_student' ,{
           url:'/update_student',
           templateUrl: 'templates/update_student.html',
           controller: 'StaffController'
         
   })
    .state('remove_student' ,{
           url:'/remove_student',
           templateUrl: 'templates/remove_student.html',
           controller: 'StaffController'
         
   })
   .state('students_details' ,{
           url:'/students_details',
	     cache: false,
           templateUrl: 'templates/students_details.html',
           controller: 'StaffController'
         
   })
     .state('upload_result' ,{
           url:'/upload_result',
           templateUrl: 'templates/upload_result.html',
           controller: 'StaffController'
         
   })
    .state('library' ,{
           url:'/library',
	   cache: false,
           templateUrl: 'templates/library.html',
           controller: 'StaffController'
         
   })
   .state('add_book' ,{
           url:'/add_book',
           templateUrl: 'templates/add_books.html',
           controller: 'StaffController'
         
   })
    .state('delete_book' ,{
           url:'/delete_book',
           templateUrl: 'templates/remove_books.html',
           controller: 'StaffController'
         
   })
  .state('show_book' ,{
           url:'/show_book',
           templateUrl: 'templates/show_books.html',
           controller: 'StaffController'
         
   })
   .state('/issued_book' ,{
           url:'/issued_book',
           templateUrl: 'templates/issued_book.html',
           controller: 'StudentController'
         
   })
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});

